<?php
class User_Model extends CI_Model {
	function __construct(){
		parent::__construct();
		
		$this->load->database();
		
	}
	function insert_user($nameuser, $email, $username, $password){
		$data = array(
			'nameuser' => $nameuser,
			'email' => $email,
			'username' => $username,
			'password' => $password);

		$this->db->insert('user', $data);
		$query = $this->db->insert_id();

		return $query;		

	}
}